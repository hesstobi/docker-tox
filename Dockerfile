FROM registry.gitlab.com/hesstobi/docker-pyenv:latest

RUN groupadd -r tox --gid=999 && \
    useradd -m -r -g tox --uid=999 tox

# Install gosu to run tox as the "tox" user instead of as root.
RUN set -eux; \
	apt-get update; \
	apt-get install --no-install-recommends -y gosu; \
	rm -rf /var/lib/apt/lists/*; \
# verify that the binary works
	gosu nobody true

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install --no-install-recommends -y nodejs && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    npm install -g junit-cli-report-viewer@1.2.0 && \
    npm cache clean --force

ENV POETRY_HOME /etc/poetry

RUN pyenv local system && \
    python -m pip install --no-cache-dir tox==3.20.0 tox-venv==0.4.0 && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python && \
    pyenv local --unset && \
    pyenv rehash

ENV PATH="/etc/poetry/bin:${PATH}"

WORKDIR /app
VOLUME /src

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["tox"]
