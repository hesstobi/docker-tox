# Docker Tox

This image contains Python versions 3.7., 3.8
It also includes [tox](https://pypi.python.org/pypi/tox) for running test automation against the various versions.
